adventofcode_2020 for Hands-on TDD study.

All following command is run from the project root folder.

Install requirements with command
"pip install -r requirements.txt"

Run test with command.
"pytest --cov-report=html --cov=. --cov-report term"

Result from test is saved in folder coverage when test is run 
with "pytest --cov-report=html --cov=. --cov-report term" and can be view by opening the file "index.html"

Run a specic day with command
python3 day_x/day_x.py
Where x is the day you want to run.

![coverage](https://gitlab.com/Geard8/adventofcode_2020/badges/main/coverage.svg?job=unit-test)
