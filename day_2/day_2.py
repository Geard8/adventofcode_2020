
def get_data(file_name):
    """
    Retrun preperad data based on str file_name.

    param: 
        file_name: str for what file name will be used to get data from
    return: list with all data from file with name based on file_name
    """
    with open(f"day_2/{file_name}", 'r') as f:
        lines = f.readlines()
    data_puzzle_input = [str(e.strip()) for e in lines]
    return data_puzzle_input

def count_correct_password_part1(password_database):
    """
    Need password_database and will return int of how many password is correct.
    A correct password is:
        The password policy indicates the lowest and highest number of times a given letter 
        must appear for the password to be valid. For example, 1-3 a means that the password must 
        contain a at least 1 time and at most 3 times.
    param: 
        password_database: list with password policy and then the password as a string 
            in format "x-y z: password" 
            where x, y is a positiv number 
            and z is one lower case letter 
            and password is any amount of lower case letters 
            example "1-3 a: abcde"
    return: int that is the amount of correct password in password_database
    """

    list_policy_password_dict = [] # list to be filled with dict what has policy and password

    for policy_password_string in password_database:
        # policy_password_dict is used to store policy and password with 
        # values min, max and char for the policy part and password for password part
        policy_password_dict = {}
        policy_password_split = policy_password_string.split()
        min_max = policy_password_split[0].split("-")
        policy_password_dict["min"] = int(min_max[0])
        policy_password_dict["max"] = int(min_max[1])
        policy_password_dict["char"] = policy_password_split[1][0]
        policy_password_dict["password"] = policy_password_split[2]
        
        list_policy_password_dict.append(policy_password_dict) # store policy_password_dict in list_policy_password_dict for later use

    count_correct_password = 0 # Will be used to track amount of correct password

    for policy_password_dict in list_policy_password_dict:
        char_count_in_password = policy_password_dict["password"].count(policy_password_dict["char"])
        if policy_password_dict["min"] <= char_count_in_password <= policy_password_dict["max"]:
            count_correct_password += 1

    print(f"We counted {count_correct_password} correct password in password database for part 1") # To show result when run to terminal
    return count_correct_password

def count_correct_password_part2(password_database):
    """
    Need password_database and will return int of how many password is correct.
    A correct password is:
        Each policy actually describes two positions in the password, 
        where 1 means the first character, 2 means the second character, and so on. 
        (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) 
        Exactly one of these positions must contain the given letter. 
        Other occurrences of the letter are irrelevant for the purposes of policy enforcement.
    param: 
        password_database: list with password policy and then the password as a string 
            in format "x-y z: password" 
            where x, y is a positiv number 
            and z is one lower case letter 
            and password is any amount of lower case letters 
            example "1-3 a: abcde"
    return: int that is the amount of correct password in password_database
    """
    list_policy_password_dict = [] # list to be filled with dict what has policy and password

    for policy_password_string in password_database:
        # policy_password_dict is used to store policy and password with 
        # values first, second and char for the policy part and password for password part
        policy_password_dict = {}
        policy_password_split = policy_password_string.split()
        min_max = policy_password_split[0].split("-")
        policy_password_dict["first"] = int(min_max[0])
        policy_password_dict["second"] = int(min_max[1])
        policy_password_dict["char"] = policy_password_split[1][0]
        policy_password_dict["password"] = policy_password_split[2]
        
        list_policy_password_dict.append(policy_password_dict) # store policy_password_dict in list_policy_password_dict for later use

    count_correct_password = 0 # Will be used to track amount of correct password

    for policy_password_dict in list_policy_password_dict:
        # TODO check for correct password and incresse count_correct_password for each that is correct.
        is_first_match = policy_password_dict["password"][policy_password_dict["first"] - 1] == policy_password_dict["char"]
        is_second_match = policy_password_dict["password"][policy_password_dict["second"] - 1] == policy_password_dict["char"]
        
        if is_first_match ^ is_second_match:
            count_correct_password += 1

    print(f"We counted {count_correct_password} correct password in password database for part 2") # To show result when run to terminal
    return count_correct_password

def main():
    data_puzzle_input = get_data("data.txt")

    success_part1 = count_correct_password_part1(data_puzzle_input)
    success_part2 = count_correct_password_part2(data_puzzle_input)
    return bool(success_part1) and bool(success_part2)

if __name__ == "__main__":
    main()
