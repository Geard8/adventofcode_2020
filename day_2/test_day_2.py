from day_2 import main, get_data, count_correct_password_part1, count_correct_password_part2
import pytest

def test_main() -> None:
    assert main() is True

def test_get_data():
    data = get_data("data.txt")
    assert data is not False

@pytest.fixture
def password_database():
    return ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"] # str values taken from AoC example day 2

class TestDay2Part1:
    def test_count_correct_password(self, password_database):
        """
        Testing with example in day 2 part 1
        Test count_correct_password_part1 what given password_database shall count correct password in password_database.
        A correct password is:
            The password policy indicates the lowest and highest number of times a given letter 
            must appear for the password to be valid. For example, 1-3 a means that the password must 
            contain a at least 1 time and at most 3 times.
        """
        correct_password_count = count_correct_password_part1(password_database)
        assert correct_password_count == 2 # 2 taken from AoC example day 2 part 1

    def test_count_correct_password_on_empty_list(self):
        # Test count_correct_password_part1 for that with a empty list it will have 0 correct password in it. 
        correct_password_count = count_correct_password_part1([])
        assert correct_password_count == 0

class TestDay2Part2:
    def test_count_correct_password(self, password_database):
        """
        Testing with example in day 2 part 2
        Test count_correct_password_part1 what given password_database shall count correct password in password_database.
        A correct password is:
            Each policy actually describes two positions in the password, 
            where 1 means the first character, 2 means the second character, and so on. 
            (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) 
            Exactly one of these positions must contain the given letter. 
            Other occurrences of the letter are irrelevant for the purposes of policy enforcement.
        """
        correct_password_count = count_correct_password_part2(password_database)
        assert correct_password_count == 1 # 1 taken from AoC example day 2 part 2

    def test_count_correct_password_on_empty_list(self):
        # Test count_correct_password_part2 for that with a empty list it will have 0 correct password in it. 
        correct_password_count = count_correct_password_part2([])
        assert correct_password_count == 0