from .day_1 import main, find_entries_that_will_be_sum_part1, get_data, find_entries_that_will_be_sum_part2
import pytest

def test_main() -> None:
    assert main() is True

def test_get_data():
    data = get_data("data.txt")
    assert data is not False

@pytest.fixture
def entries():
    return [1721, 979, 366, 299, 675, 1456] # int values taken from AoC example day 1 part 1

@pytest.fixture
def sum():
    return 2020 # 2020 taken from AoC example day 1 part 1

class TestDay1Part1:
    def test_example_find_sum_2020(self, entries, sum):
        # Testing with example in day 1 part 1
        # Where given a list if numbers will find what two entries is will lead to sum when added together
        
        entry_1, entry_2 = find_entries_that_will_be_sum_part1(entries, sum)
        assert entry_1 == 1721 # 1721 taken from AoC example
        assert entry_2 == 299 # 299 taken from AoC example

    def test_example_check_answer(self, entries, sum):
        # Testing with example in day 1 part 1
        # Test to get correct answer by multiply the two entries from fuction find_entries_that_will_be_sum_part1

        entry_1, entry_2 = find_entries_that_will_be_sum_part1(entries, sum)
        assert entry_1 * entry_2 == 514579 # 514579 taken from AoC example

    def test_find_entries_that_will_be_sum_part1_with_empty_list(self):
        assert find_entries_that_will_be_sum_part1([], 0) is False

class TestDay1Part2:
    def test_example_find_sum_2020(self, entries, sum):
        # Testing with example in day 1 part 2
        # Where given a list if numbers will find what three entries is will lead to sum when added together
        
        entry_1, entry_2, entry_3 = find_entries_that_will_be_sum_part2(entries, sum)
        assert entry_1 == 979 # 979 taken from AoC example
        assert entry_2 == 366 # 366 taken from AoC example
        assert entry_3 == 675 # 675 taken from AoC example

    def test_example_check_answer(self, entries, sum):
        # Testing with example in day 1 part 2
        # Test to get correct answer by multiply the two entries from fuction find_entries_that_will_be_sum_part1

        entry_1, entry_2, entry_3 = find_entries_that_will_be_sum_part2(entries, sum)
        assert entry_1 * entry_2 * entry_3 == 241861950 # 241861950 taken from AoC example

    def test_find_entries_that_will_be_sum_part1_with_empty_list(self):
        assert find_entries_that_will_be_sum_part2([], 0) is False
