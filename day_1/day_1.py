
def get_data(file_name):
    """
    Retrun preperad data based on str file_name.

    param: 
        file_name: str for what file name will be used to get data from
    return: list with all data from file with name based on file_name
    """
    with open(f"day_1/{file_name}", 'r') as f:
        lines = f.readlines()
    data_puzzle_input = [int(e.strip()) for e in lines]
    return data_puzzle_input


def find_entries_that_will_be_sum_part1(entries, sum):
    """
    Need entries and sum to find what two int value in entries are added together to value of sum.

    param: 
        entries: list with int values in it.
        sum: int
    return: int, int the two entry that added together is sum
    """

    while entries:
        entry_1 = entries[0]
        entries.pop(0) # removes first entry in entries so it won't try to check if it can be added with it self to be sum.
        for entry in entries:
            if entry + entry_1 == sum:
                print(f"Calculate answer with {entry_1} * {entry} = {entry_1 * entry}") # Print answer as readable output.

                return entry_1, entry

    print("did not found any two entries added together to value of sum.")
    return False

def find_entries_that_will_be_sum_part2(entries, sum):
    """
    Need entries and sum to find what three int value in entries are added together to value of sum.

    param: 
        entries: list with int values in it.
        sum: int
    return: int, int the three entry that added together is sum
    """

    while entries:
        entry_1 = entries[0]
        entries.pop(0) # removes first entry in entries so it won't try to check if it can be added with it self to be sum.
        entries_copy = entries.copy()
        while entries_copy:
            entry_2 = entries_copy[0]
            entries_copy.pop(0) # removes first entry in entries so it won't try to check if it can be added with it self to be sum.
            for entry_3 in entries_copy:
                if entry_3 + entry_1 + entry_2 == sum:
                    # Print answer as readable output.
                    print(f"Calculate answer with {entry_1} * {entry_2} * {entry_3} = {entry_1 * entry_2 * entry_3}") 
                    return entry_1, entry_2, entry_3



    print("did not found any three entries added together to value of sum.")
    return False

def main():
    """ 
    Get data_puzzle_input from file data.txt and use function find_entries_that_will_be_sum that 
    will print out the answer if there is one.
    File data.txt most be in day_1 folder.

    return: True if all when well otherwise False.
    """
    data_puzzle_input = get_data("data.txt")

    # Here we will get the answers for part 1 and part 2 based on data_puzzle_input we got from file data.txt
    # The answers will be printed out in terminalen.
    success_part1 = find_entries_that_will_be_sum_part1(data_puzzle_input, 2020) 
    success_part2 = find_entries_that_will_be_sum_part2(data_puzzle_input, 2020) 
    return bool(success_part1) and bool(success_part2)

if __name__ == "__main__":
    main()
